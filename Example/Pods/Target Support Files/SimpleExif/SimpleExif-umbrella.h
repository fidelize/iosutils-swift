#import <UIKit/UIKit.h>

#import "ExifContainer.h"
#import "UIImage+Exif.h"

FOUNDATION_EXPORT double SimpleExifVersionNumber;
FOUNDATION_EXPORT const unsigned char SimpleExifVersionString[];

